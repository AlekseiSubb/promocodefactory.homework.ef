﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    [Index("FirstName", "LastName", IsUnique = true, Name = "Employee_FirstName_LastName_Index")]
    public class Employee
        : BaseEntity
    {
        [Key]
        public Guid Id {get;set;}

        [MaxLength(100)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(100)]
        [Required]
        public string LastName { get; set; }

        [IgnoreDataMember]
        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(100)]
        public string? Email { get; set; }

        //public Guid RoleId { get; set; }
        public Role? Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}