﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
   [Index("Name", IsUnique = true, Name = "Role_Name_Index")]
    public class Role  : BaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [MaxLength(100)]
        public string? Description { get; set; }

        public List<Employee> Employees { get; set; }
    }
}