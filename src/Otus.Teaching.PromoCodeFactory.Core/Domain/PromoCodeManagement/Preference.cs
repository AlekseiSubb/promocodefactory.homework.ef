﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Index("Name", IsUnique = true, Name = "Preference_Name_Index")]
    public class Preference
        : BaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        public List<Customer> Customers { get; set; }
    }
}