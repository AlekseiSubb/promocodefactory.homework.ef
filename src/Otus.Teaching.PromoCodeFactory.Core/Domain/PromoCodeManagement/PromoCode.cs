﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
     [Index("Code", "PartnerName", IsUnique = true, Name = "PromoCode_Code_PartnerName_Index")]
    public class PromoCode
        : BaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Code { get; set; }

        [MaxLength(100)]
        [Required]
        public string PartnerName { get; set; }

        public string? ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        //public Guid EmployeeId { get; set; }
        public Employee PartnerManager { get; set; }

        //public Guid PreferenceId { get; set; }
        public Preference Preference { get; set; }

        //public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}