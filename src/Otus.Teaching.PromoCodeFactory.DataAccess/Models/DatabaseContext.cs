﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using SQLitePCL;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Models
{
    /// <summary>
    /// Контекст
    /// </summary>
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
            InitDb();
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            /*
             * через Fluent API:
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Preferences)
                .WithMany(p => p.Customers)
                .UsingEntity(j => j.ToTable("CustomerPreference"));

            modelBuilder.Entity<Employee>().Property(e => e.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(e => e.LastName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(e => e.Email).HasMaxLength(100);

            modelBuilder.Entity<Role>().Property(r => r.Name).HasMaxLength(100);
            modelBuilder.Entity<Role>().Property(r => r.Description).HasMaxLength(100);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);

            modelBuilder.Entity<Preference>().Property(c => c.Name).HasMaxLength(100);    
            */
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }
        public void InitDb()
        {
            //загрузка начальных данных
            Roles.AddRange(FakeDataFactory.Roles);
            Preferences.AddRange(FakeDataFactory.Preferences);
            SaveChanges();

            var EmployeesBase = FakeDataFactory.Employees.ToList();
            EmployeesBase.ForEach(e => 
                                    {
                                        if (e.Role != null && e.Role.Name != null)
                                            e.Role = Roles.FirstOrDefault(x => x.Name == e.Role.Name);
                                        else
                                            e.Role = null;
                                    });
            Employees.AddRange(EmployeesBase);

            var CustomersBase = FakeDataFactory.Customers.ToList();
            CustomersBase.ForEach(c =>
            {
                if (c.Preferences != null && c.Preferences.Count > 0)
                {
                    c.Preferences.ForEach(p =>
                    {
                        p = Preferences.FirstOrDefault(x => x.Name == p.Name);
                    });
                }
                if (c.Promocodes != null && c.Promocodes.Count > 0)
                {
                    c.Promocodes.ForEach(p =>
                    {
                        p = PromoCodes.FirstOrDefault(x => x.Code == p.Code && x.PartnerName == p.PartnerName);
                    });
                }
            });
            Customers.AddRange(CustomersBase);
            SaveChanges();

            var PromoCodesBase = FakeDataFactory.PromoCodes.ToList();
            PromoCodesBase.ForEach(pc =>
            {
                if (pc.PartnerManager != null && pc.PartnerManager.FirstName != null && pc.PartnerManager.LastName != null)
                    pc.PartnerManager = Employees.FirstOrDefault(x => x.LastName == pc.PartnerManager.LastName
                                                                    && x.FirstName == pc.PartnerManager.FirstName);
                else
                    pc.PartnerManager = null;

                if (pc.Preference != null && pc.Preference.Name != null)
                    pc.Preference = Preferences.FirstOrDefault(x => x.Name == pc.Preference.Name);
                else
                    pc.Preference = null;

                if (pc.Customer != null && pc.Customer.FirstName != null && pc.Customer.LastName != null)
                    pc.Customer = Customers.FirstOrDefault(x => x.LastName == pc.Customer.LastName
                                                                    && x.FirstName == pc.Customer.FirstName);
                else
                    pc.Customer = null;

            });
            PromoCodes.AddRange(PromoCodesBase);
            SaveChanges();
        }
    }
}