﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;
using SQLitePCL;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> 
    : IRepository<T>
    where T : BaseEntity
    {

        private readonly DatabaseContext _db;

        public EfRepository(DatabaseContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _db.Set<T>().ToListAsync();
            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var item = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

            return item;
        }

        public async Task<bool> DeleteByIdAsync(Guid id)
        {
            bool res = false;
            var item = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if (item != null)
            {
                _db.Set<T>().Remove(item);
                await _db.SaveChangesAsync();
                res =  true;
            }

            return res;
        }

        public async Task<Guid> CreateAsync(T newItem)
        {
            var itemEf = await _db.Set<T>().AddAsync(newItem);
            if (itemEf is { })
            {
                await _db.SaveChangesAsync();
                return itemEf.Entity.Id;
            }

            return Guid.Empty;
        }

        public async Task<bool> UpdateAsync(T newItem)
        { 
            var itemEf = _db.Set<T>().Update(newItem);
            if (itemEf is { })
            {
                await _db.SaveChangesAsync();
                return true;
            }

            return false;
        }
    }
}
