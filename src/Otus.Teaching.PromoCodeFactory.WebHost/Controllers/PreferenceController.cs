﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения покупателей
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase 
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PreferenceController(IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository)
        {
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }

        /// <summary>
        /// Получить все возможные предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PreferenceResponse>> GetRolesAsync()
        {
            var preferences = await _preferencesRepository.GetAllAsync();

            var preferencesModelList = preferences.Select(x =>
                new PreferenceResponse()
                {
                    Name = x.Name
                }).ToList();

            return preferencesModelList;
        }


        /// <summary>
        /// Получить предпочтение по id покупателя
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<IEnumerable<PreferenceResponse>> GetRoleAsync(Guid id)
        {
            var preferences = await _preferencesRepository.GetAllAsync();
            var customer = await _customersRepository.GetByIdAsync(id);


            var preferencesModelList = preferences.Where(p => p.Customers.Contains(customer)).Select(x =>
                new PreferenceResponse()
                {
                    Name = x.Name
                }).ToList();

            return preferencesModelList;
        }
    }
}


