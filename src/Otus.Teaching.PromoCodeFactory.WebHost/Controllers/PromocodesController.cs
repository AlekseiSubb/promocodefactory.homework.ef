﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{

    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Preference> _preferencesRepository;

        public PromocodesController(IRepository<PromoCode> promocodesRepository, IRepository<Customer> customersRepository, IRepository<Preference> preferencesRepository)
        {
            _promocodesRepository = promocodesRepository;
            _customersRepository = customersRepository;
            _preferencesRepository = preferencesRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodesRepository.GetAllAsync();

            var promocodesModelList = promocodes.Select(x =>
                new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName
                })
            .ToList();

            return promocodesModelList;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //получим предпочтение
            var preferences = await _preferencesRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault(x => x.Name == request.Preference);
            if(preference == null)
                return NotFound($"Preference: {request.Preference} not found!");

            //создаем промокод
            var pc = new PromoCode {
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                Preference = preference
            };
            var resInsertPC = await _promocodesRepository.CreateAsync(pc);
            if (resInsertPC == Guid.Empty)
                return BadRequest($"Promocode: {request.PromoCode} PartnerName: {request.PartnerName} not Inserted!");

            //получим пользователей с указанным предпочтением и добавить им новый промокод
            var customers = await _customersRepository.GetAllAsync();
            var customersEdit = customers.Where(c => c.Preferences.Contains(preference));
            customersEdit.ToList().ForEach(x =>
            {
                x.Promocodes.Add(pc);
            });

            //изменить пользователя
            customersEdit.ToList().ForEach(async x =>
            {
                await _customersRepository.UpdateAsync(x);
            });

            return Ok("Promocode added To Customers With Preference");
        }
    }
}