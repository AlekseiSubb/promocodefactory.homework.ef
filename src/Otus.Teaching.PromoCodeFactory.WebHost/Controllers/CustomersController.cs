﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            // Добавить получение списка клиентов
            var customers = await _customerRepository.GetAllAsync();
            var result = customers.Select(c =>
                new CustomerResponse
                {
                    Id = c.Id,
                    FirstName = c.FirstName!,
                    LastName = c.LastName!,
                    Email = c.Email!,
                    PromoCodes = c.Promocodes?
                        .Select(pc => new PromoCodeShortResponse
                        {
                            Id = pc.Id,
                            Code = pc.Code!,
                            ServiceInfo = pc.ServiceInfo!,
                            BeginDate = pc.BeginDate.ToString(),
                            EndDate = pc.EndDate.ToString(),
                            PartnerName = pc.PartnerName!
                        }).ToList()!,
                    Preferences = c.Preferences?
                        .Select(preference => new PreferenceResponse
                        {
                            Name = preference.Name!
                        }).ToList()
                }).ToList();

            return Ok(result);
        }

        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            // Добавить получение клиента вместе с выданными ему промомкодами
            var customer = await _customerRepository.GetByIdAsync(id);

            var result = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName!,
                LastName = customer.LastName!,
                Email = customer.Email!,
                PromoCodes = customer.Promocodes?
                    .Select(code => new PromoCodeShortResponse
                    {
                        Id = code.Id,
                        Code = code.Code!,
                        ServiceInfo = code.ServiceInfo!,
                        BeginDate = code.BeginDate.ToString(),
                        EndDate = code.EndDate.ToString(),
                        PartnerName = code.PartnerName!
                    }).ToList()!,
                Preferences = customer.Preferences?
                    .Select(preference => new PreferenceResponse
                    {
                        Name = preference.Name!
                    }).ToList()
            };

            return Ok(result);
        }

        /// <summary>
        /// Создание нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            // Добавить создание нового клиента вместе с его предпочтениями
            var preferences = (await _preferenceRepository.GetAllAsync())
                .Where(x =>
                    request.PreferenceIds.Any(y => y.Equals(x.Id))).ToList();

            var createEntry = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences
            };
            var result = await _customerRepository.CreateAsync(createEntry);
            return Ok(result);
        }

        /// <summary>
        /// Редактирование клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями

            //получем сначала customer 
            var customer = await _customerRepository.GetByIdAsync(id);

            customer.Preferences.Clear();

            var preference = await _preferenceRepository.GetAllAsync();
            request.PreferenceIds.ForEach(id =>
            {
                var item = preference.FirstOrDefault(p => p.Id == id);
                if (item != null)
                    customer.Preferences.Add(item);
            });

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            var res = await _customerRepository.UpdateAsync(customer);

            if (res)
                return Ok(customer);
            else
                return NotFound();
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами

            //получем сначала customer 
            var customer = await _customerRepository.GetByIdAsync(id);
            //удалим все промокоды и предпочтения данного покупателя
            customer.Promocodes.Clear();
            customer.Preferences.Clear();
            var res = await _customerRepository.UpdateAsync(customer);

            //Удаляем покупателя
            var res2 = await _customerRepository.DeleteByIdAsync(id);
            if (res && res2)
                return Ok($"Role with id = {id} deleted");
            else
                return NotFound(id);
        }
    }
}