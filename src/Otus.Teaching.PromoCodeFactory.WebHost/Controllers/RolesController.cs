﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> efRepository)
        {
            _rolesRepository = efRepository;
        }

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => 
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }


        /// <summary>
        /// Получить роль по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleItemResponse>> GetRoleAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            var rolesModel = new RoleItemResponse()
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description
            };

            return rolesModel;
        }

        /// <summary>
        /// Удалить роль по Id
        /// </summary>
        /// <returns></returns>
        [HttpPost("{id:guid}")]
        // [HttpDelete]
        public async Task<IActionResult> DeleteRoleByIdAsync(Guid id)
        {
            var res = await _rolesRepository.DeleteByIdAsync(id);

            if (res)
                return Ok($"Role with id = {id} deleted");
            else
                return NotFound(id);
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateRoleAsync(Role item)
        {
            //БД добавляет сотрудника и возвращает id добавленной записи
            var id = await _rolesRepository.CreateAsync(item);

            if (id == Guid.Empty)
                return NotFound(item);
            else
                return Ok($"Role {item.Name} add");
        }

        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateRoleAsync(Role item)
        {
            //БД изменяет роль и возвращает true если удачно
            var res = await _rolesRepository.UpdateAsync(item);

            if (res)
                return Ok($"Role: {item.Name} updated");
            else
                return NotFound();
        }
    }
}