﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Settings
{
    public class ApplicationSettings
    {
        public string ConnectionString { get; set; }
        public string ConnectionStringSQLServer { get; set; }
    }
}
